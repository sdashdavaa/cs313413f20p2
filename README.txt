Project 2 Sugar Dashdavaa

*TestIterator
-TODO also try with a LinkedList - does it make any difference behaviorally?
  list = new LinkedList<Integer>(); there will be no such difference

-TODO what happens if you use list.remove(Integer.valueOf(77))?
 it will throw ConcurrentModificationException error and it will not compile

*TestList
-TODO also try with a LinkedList - does it make any difference behaviorally?
It won't make any difference

-list.remove(5); // TODO answer: what does this method do?
removes element 77 at index 5

-list.remove(Integer.valueOf(5)); // TODO answer: what does this one do?
removes element at index 5 which is 6

*TestPerformance
 // TODO answer: which of the two lists performs better for each method as the size increases?
LinkedList